﻿using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using HRMS;
using Shared.Controller;
using Shared.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared
{
    public class QualificationFragment:Fragment
    {
        private string employeeId;
      //  private List<QualificationModel> qualificationDetails;

        public static QualificationFragment NewInstance(string employeeId)
        {
            QualificationFragment qualityFragment = new QualificationFragment();
            Bundle args = new Bundle();
            args.PutString("employeeId", employeeId);
            qualityFragment.Arguments = args;
            return qualityFragment;

        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            employeeId = Arguments.GetString("employeeId");
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.QualificationLayout, container, false);
            TextView qualificationText, institutionText, passOutText, percentageText;
            qualificationText = view.FindViewById<TextView>(Resource.Id.qualificationText);
            institutionText = view.FindViewById<TextView>(Resource.Id.institutionText);
            passOutText = view.FindViewById<TextView>(Resource.Id.passOutText);
            percentageText = view.FindViewById<TextView>(Resource.Id.percentageText);
            //qualificationDetails = new QualificationDetails().getQualification();
            //var qualification = new QualificationDetails().getQualification().Find(x => x.EmployeeId == employeeId);
            //qualificationText.Text = qualification.Qualifiaction;
            //institutionText.Text = qualification.InstituteName;
            //passOutText.Text = qualification.PassOutYear.ToString();
            //percentageText.Text = qualification.Percentage;
            return view;
        }

        
    }
}
