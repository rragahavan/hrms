﻿using Android.Support.V4.App;
using System;
using System.Collections.Generic;
using System.Text;
using Android.OS;
using Android.Views;
using HRMS;

namespace Shared.Fragments
{
    public class ApplyLeaveFragment : Fragment
    {
        private int employeeId;
        public static ApplyLeaveFragment NewInstance(int employeeId)
        {
            ApplyLeaveFragment applyLeaveFragment = new ApplyLeaveFragment();
            Bundle args = new Bundle();
            args.PutInt("employeeId", employeeId);
            applyLeaveFragment.Arguments = args;
            return applyLeaveFragment;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            employeeId = Arguments.GetInt("employeeId");
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.ApplyLeaveLayout, container, false);
            return view;
        }
    }
}
