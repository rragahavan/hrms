using System.Collections.Generic;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;
using Shared.Model;
using Shared.Controller;

namespace HRMS.Controller
{
    public class AddressFragment : Fragment
    {
        private string employeeId;
        private List<Employees> employeeDetails;

        public static AddressFragment NewInstance(string employeeId)
        {
            AddressFragment addressFragment = new AddressFragment();
            Bundle args = new Bundle();
            args.PutString("employeeId", employeeId);
            addressFragment.Arguments = args;
            return addressFragment;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            employeeId = Arguments.GetString("employeeId");
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            
            View view = inflater.Inflate(Resource.Layout.AddressLayout, container, false);
            TextView address, city, state, country;
            employeeDetails = new EmployeeDetails().getEmployee();
            var employee = new EmployeeDetails().getEmployee().Find(x => x.empcode == employeeId);
            address = view.FindViewById<TextView>(Resource.Id.addressText);
            city = view.FindViewById<TextView>(Resource.Id.cityText);
            state = view.FindViewById<TextView>(Resource.Id.stateText);
            country = view.FindViewById<TextView>(Resource.Id.countryText);
            address.Text = employee.email;
            //city.Text = employee.City;
            //state.Text = employee.State;
            //country.Text = employee.Country;
           
            return view;
        }
    }
}