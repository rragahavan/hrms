﻿using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;
using HRMS;
using Shared.Controller;
using Shared.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Fragments
{
    class ExperienceFragment:Fragment
    {
        private string employeeId;
        Animation anim;
        //private List<ExperienceModel> experienceDetails;

        public static ExperienceFragment NewInstance(string employeeId)
        {
            ExperienceFragment experienceFragment = new ExperienceFragment();
            Bundle args = new Bundle();
            args.PutString("employeeId", employeeId);
            experienceFragment.Arguments = args;
            return experienceFragment;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            employeeId = Arguments.GetString("employeeId");
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.ExperienceLayout, container, false);
            TextView companyName, startDate, endDate;
            companyName = view.FindViewById<TextView>(Resource.Id.companyNameText);
            startDate = view.FindViewById<TextView>(Resource.Id.starDateText);
            endDate = view.FindViewById<TextView>(Resource.Id.EndDateText);
           // experienceDetails = new ExperienceDetails().getExperienceList();
            //var experince = new ExperienceDetails().getExperienceList().Find(x => x.EmployeeId == employeeId);
            //companyName.Text = experince.CompanyName;
            //startDate.Text = experince.StartDate;
            //endDate.Text = experince.EndDate;
            
            
            return view;
        }
    }
}
