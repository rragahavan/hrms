﻿
using System;
using System.Collections.Generic;
using System.Text;
using Android.OS;
using Android.Views;
using HRMS;
using Android.Support.V4.App;
using Android.Widget;
using HRMS.Classes;

namespace Shared.Fragments
{
    public class MyLeaveFragment:Fragment
    {
        private int employeeId;
        public static MyLeaveFragment NewInstance(int employeeId)
        {
            MyLeaveFragment myLeaveFragment = new MyLeaveFragment();
            Bundle args = new Bundle();
            args.PutInt("employeeId", employeeId);
            myLeaveFragment.Arguments = args;
            return myLeaveFragment;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            employeeId = Arguments.GetInt("employeeId");
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.MyLeaveLayout, container, false);
           
            
            return view;
        }

    }
}
