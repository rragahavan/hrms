using System.Collections.Generic;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;
using Shared.Model;
using Shared.Controller;

namespace HRMS.Controller
{
    public class BasicInformationFragment : Fragment
    {
        private int employeeId;
      //  private List<Employee> employeeDetails;

        public static BasicInformationFragment NewInstance(int employeeId)
        {
            BasicInformationFragment secondFragment = new BasicInformationFragment();
            Bundle args = new Bundle();
            args.PutInt("employeeId", employeeId);
            secondFragment.Arguments = args;
            return secondFragment;

        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            employeeId = Arguments.GetInt("employeeId");
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            View view = inflater.Inflate(Resource.Layout.BasicInformation, container, false);
            TextView firstName, lastName, jobTitle;
            firstName = view.FindViewById<TextView>(Resource.Id.firstNameText);
            lastName = view.FindViewById<TextView>(Resource.Id.lastNameText);
            jobTitle = view.FindViewById<TextView>(Resource.Id.jobTitleText);
            //employeeDetails = new EmployeeDetails().getEmployee();
            //var employee = employeeDetails.Find(x => x.EmployeeId == employeeId);
            //var employee = new EmployeeDetails().getEmployee().Find(x => x.EmployeeId == employeeId);

            //firstName.Text = employee.FirstName;
            //lastName.Text = employee.LastName;
            //jobTitle.Text = employee.Designation;
            return view;
        }
    }
}