using Android.OS;
using Android.Views;
using Android.Support.V4.App;
using System.Collections.Generic;
using Shared.Model;
using Shared.Controller;
using Android.Widget;

namespace HRMS.Controller
{
    public class SkillFragment : Fragment
    {
        private string employeeId;
        //private List<SkillModel> skillDetails;


        public static SkillFragment NewInstance(string employeeId)
        {
            SkillFragment skillFragment = new SkillFragment();
            Bundle args = new Bundle();
            args.PutString("employeeId", employeeId);
            skillFragment.Arguments = args;
            return skillFragment;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            employeeId = Arguments.GetString("employeeId");
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.SkillsLayout, container, false);
            TextView skillName, skillExperience;
            skillName = view.FindViewById<TextView>(Resource.Id.skillNameText);
            skillExperience = view.FindViewById<TextView>(Resource.Id.skillExperienceText);
            //skillDetails = new SkillDetails().getSkillDetails();
            //var skills = new SkillDetails().getSkillDetails().Find(x => x._id == employeeId);
            //skillName.Text = skills.SkillName;
            //skillExperience.Text = skills.SkillExperience;
            return view;
        }
    }
}