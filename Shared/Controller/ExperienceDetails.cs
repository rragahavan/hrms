﻿using Shared.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Controller
{
    public class ExperienceDetails
    {
        public List<ExperienceModel> getExperienceList()
        {
            List<ExperienceModel> experienceList=new List<ExperienceModel>();
            experienceList.Add(new ExperienceModel { EmployeeId =1, CompanyName ="Microsoft", StartDate ="5-10-2010", EndDate ="8-10-2014"});
            experienceList.Add(new ExperienceModel { EmployeeId =2, CompanyName ="Infosys", StartDate ="15-10-2012", EndDate ="11-10-2014"});
            experienceList.Add(new ExperienceModel { EmployeeId =3, CompanyName ="Goavega", StartDate ="15-10-2014", EndDate ="8-11-2015"});
            experienceList.Add(new ExperienceModel { EmployeeId =4, CompanyName ="IBM", StartDate ="02-06-2008", EndDate ="03-03-2011"});
            experienceList.Add(new ExperienceModel { EmployeeId =5, CompanyName ="Infosys", StartDate ="04-08-2014", EndDate ="05-05-2015"});
            experienceList.Add(new ExperienceModel { EmployeeId =6, CompanyName ="Goavega", StartDate ="22-02-2000", EndDate ="02-09-2014"});
            experienceList.Add(new ExperienceModel { EmployeeId =7, CompanyName ="Microsoft", StartDate ="25-06-2005", EndDate ="10-02-2009"});
            experienceList.Add(new ExperienceModel { EmployeeId =8, CompanyName ="Wipro", StartDate ="18-08-2013", EndDate ="01-08-2015"});
            return experienceList;
        }
    }
}
