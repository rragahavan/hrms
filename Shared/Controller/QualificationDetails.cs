﻿using Shared.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Controller
{
    public class QualificationDetails
    {
        public List<QualificationModel> getQualification()
        {
            List<QualificationModel> qualificationList = new List<QualificationModel>();
            qualificationList.Add(new QualificationModel { EmployeeId =1, Qualifiaction ="B.E", InstituteName ="NIIT", PassOutYear =2007, Percentage ="50%"});
            qualificationList.Add(new QualificationModel { EmployeeId =2, Qualifiaction ="MCA", InstituteName ="MIIT", PassOutYear =2008, Percentage ="60%"});
            qualificationList.Add(new QualificationModel { EmployeeId =3, Qualifiaction ="BCA", InstituteName ="SIT", PassOutYear =2009, Percentage ="75%"});
            qualificationList.Add(new QualificationModel { EmployeeId =4, Qualifiaction ="BCA", InstituteName ="RNSIT", PassOutYear =2010, Percentage ="85%"});
            qualificationList.Add(new QualificationModel { EmployeeId =5, Qualifiaction ="MCA", InstituteName ="NIIT", PassOutYear =2005, Percentage ="65%"});
            qualificationList.Add(new QualificationModel { EmployeeId =6, Qualifiaction ="BE", InstituteName ="RNSIT", PassOutYear =2000, Percentage ="70%"});
            qualificationList.Add(new QualificationModel { EmployeeId =7, Qualifiaction ="MCA", InstituteName ="MIIT", PassOutYear =2015, Percentage ="73%"});
            qualificationList.Add(new QualificationModel { EmployeeId =8, Qualifiaction ="MCA", InstituteName ="SIT", PassOutYear =2013, Percentage ="62%"});
            return qualificationList;

        }
    }
}
