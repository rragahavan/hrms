﻿using Newtonsoft.Json;
using RestSharp;
using Shared.Model;
using System.Collections.Generic;

namespace Shared.Controller
{
    public class EmployeeDetails
    {
        
        public List<Employees> getEmployee()
        {
            List<Employees> list = new List<Employees>();
            var client = new RestClient("http://192.168.1.118:3000");
            var request = new RestRequest("api/invoke", Method.POST);
            request.RequestFormat = DataFormat.Json;
            var body = new
            {
                service = "hrms",
                action = "employeelist"
            };
            request.AddBody(body);
            var response = client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                list = JsonConvert.DeserializeObject<RootObject>(response.Content).data;
            }
            return list;
        }
    }
}
