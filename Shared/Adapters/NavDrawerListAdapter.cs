using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;
using HRMS.Model;
using HRMS;

namespace HRMS.Controller
{
    class NavDrawerListAdapter : BaseAdapter
    {
        private Context context;
        private List<NavDrawerItem> navDrawerItems;
        public NavDrawerListAdapter(Context context, List<NavDrawerItem> navDrawerItems)
        {
            this.context = context;
            this.navDrawerItems = navDrawerItems;
        }

        public override int Count
        {
            get
            {
                return navDrawerItems.Count;
            }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return null;
           
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override Android.Views.View GetView(int position, Android.Views.View convertView, ViewGroup parent)
        {
            var item = this.navDrawerItems[position];
            if (convertView == null)
            {
                LayoutInflater mInflater = (LayoutInflater)context.GetSystemService(Activity.LayoutInflaterService);
                convertView = mInflater.Inflate(Resource.Layout.drawer_list_item, parent, false);
            }

            TextView txtTitle = (TextView)convertView.FindViewById(Resource.Id.title);
            txtTitle.SetText(item.title, TextView.BufferType.Normal);
            return convertView;
        }
    }
}