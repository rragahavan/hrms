﻿using Android.Runtime;
using Android.Support.V4.App;
using Java.Lang;
using Shared.Fragments;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Adapters
{
    public class LeaveAdapter : FragmentPagerAdapter
    {
        private int Num_Tabs = 2;
        private string[] tabTitles = { "My Leave", "Apply Leave"};
        private int employeeId;

        public LeaveAdapter(Android.Support.V4.App.FragmentManager fm, int employeeId) : base(fm)
        {
            this.employeeId = employeeId;
        }
        public override int Count
        {
            get
            {
                return Num_Tabs;
            }
        }

        public override ICharSequence GetPageTitleFormatted(int position)
        {

            return CharSequence.ArrayFromStringArray(tabTitles)[position];

        }

        public override Fragment GetItem(int position)
        {
            switch (position)
            {
                case 0:
                    return MyLeaveFragment.NewInstance(employeeId);
                case 1:
                    return ApplyLeaveFragment.NewInstance(employeeId);
               default:
                    return null;
            }
        }

    }
}
