using Android.Runtime;
using Android.Support.V4.App;
using Java.Lang;
using Shared;
using Shared.Fragments;

namespace HRMS.Controller
{
    public class MyPagerAdapter : FragmentPagerAdapter
    {
        private static int NUM_ITEMS = 4;
        
        private string[] tabTitles = { "Address","Skills","Qualification","Experience" };
        private string employeeId;

        public MyPagerAdapter(Android.Support.V4.App.FragmentManager fm,string employeeId) : base(fm)
        {
            this.employeeId = employeeId;
        }

        public override int Count
        {
            get
            {
                return NUM_ITEMS;
            }
        }

        public override ICharSequence GetPageTitleFormatted(int position)
        {

           return CharSequence.ArrayFromStringArray(tabTitles)[position];
            
        }

        public override Android.Support.V4.App.Fragment GetItem(int position)
        {
            switch (position)
            {
                case 0:
                    return AddressFragment.NewInstance(employeeId);
                case 1:
                    return SkillFragment.NewInstance(employeeId);
                case 2:
                    return QualificationFragment.NewInstance(employeeId);
                case 3:
                    return ExperienceFragment.NewInstance(employeeId);
                default:
                    return null;
            }
        }
    }
}