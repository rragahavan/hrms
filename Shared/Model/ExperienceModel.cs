﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Model
{
    public class ExperienceModel
    {
        public int EmployeeId { get; set; }
        public string CompanyName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}
