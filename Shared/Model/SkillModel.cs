﻿namespace Shared.Model
{
    public class SkillModel
    {
        public int EmployeeId { get; set; }
        public string SkillName{ get; set; }
        public string SkillExperience{ get; set; }
        
    }
}
