﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Model
{
    public class QualificationModel
    {
        public int EmployeeId { get; set; }
        public string Qualifiaction { get; set; }
        public string InstituteName { get; set; }
        public int PassOutYear  { get; set; }
        public string Percentage { get; set; }
    }
}
