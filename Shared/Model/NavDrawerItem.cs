using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace HRMS.Model
{
    class NavDrawerItem
    {
        public NavDrawerItem()
        {

        }
        public NavDrawerItem(String title)
        {
            this.title = title;
        }
       
        public string title { get; set; }
        
    }
}