﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Model
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ProfileId { get; set; }
        public string Designation { get; set; }
        public string EmailAddress { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
    }
}
