﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Model
{
    //public class Role
    //{
       
      //  public string[] id { get; set; }
    //    public string name { get; set; }
    //}

    public class Skill
    {
        public string skillname { get; set; }
        public string experience { get; set; }
    }

    public class Currentaddress
    {
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
    }

    public class Permenantaddress
    {
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
    }

    public class Employees
    {
        
        public string _id { get; set; }
        public string empcode { get; set; }
        public string email { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string role { get; set; }
        public string managerId { get; set; }
        public string joiningdate { get; set; }
        public string emptype { get; set; }
        public string shiftstate { get; set; }
        public string grade { get; set; }
        public string nationality { get; set; }
        public string notes { get; set; }
        public string profileImage { get; set; }
       // public List<Role> roles { get; set; }
        public List<Skill> skills { get; set; }
        public List<object> experience { get; set; }
        public List<object> qualifications { get; set; }
        public string jobtitle { get; set; }
        public Currentaddress currentaddress {get; set; }
        public Permenantaddress permenantaddress { get; set; }

    }
    public class RootObject
    {
        public bool success { get; set; }
        public List<Employees> data { get; set; }
    }
}
