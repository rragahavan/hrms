
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;

namespace HRMS.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "LmsActivity")]
    public class LmsActivity : NavigationDrawerActivity
    {
        TextView toolbarTitle;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Lms);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.hamburgers);
            set(Resources.GetStringArray(Resource.Array.nav_drawer_items));
            toolbarTitle = FindViewById<TextView>(Resource.Id.toolbarTitle);
            toolbarTitle.Text = Resources.GetString(Resource.String.Lms);
        }
        public override void OnBackPressed()
        {
            Intent i = new Intent(this, typeof(MainActivity));
            StartActivity(i);
            Finish();

        }

    }
}