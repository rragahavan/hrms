
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Shared.Controller;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V4.View;
using HRMS.Controller;
using Android.Support.V4.App;
using Android.Views.Animations;
using HRMS.Classes;

namespace HRMS.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "EmployeeDetailsActivity")]
    public class EmployeeDetailsActivity : NavigationDrawerActivity
    {
        
        FragmentPagerAdapter myPagerAdapter;
        TextView toolbarTitle, nameText, employeeIdText, emailAddressText, roleText;
        ImageView profileImage;
        ViewPager viewPager;
        Animation anim;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.EmployeeDetails);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.hamburgers);
            toolbarTitle = FindViewById<TextView>(Resource.Id.toolbarTitle);
            toolbarTitle.Text = Resources.GetString(Resource.String.EmployeeDetails);
            set(Resources.GetStringArray(Resource.Array.nav_drawer_items));
            nameText = FindViewById<TextView>(Resource.Id.NameText);
            employeeIdText = FindViewById<TextView>(Resource.Id.employeeIdText);
            roleText = FindViewById<TextView>(Resource.Id.roleText);
            emailAddressText = FindViewById<TextView>(Resource.Id.emailAddressText);
            profileImage = FindViewById<ImageView>(Resource.Id.profileImage);
            string employeeId = Intent.GetStringExtra("employeeId");
            var employee = new EmployeeDetails().getEmployee().
                Find(x => x.empcode ==employeeId);
            nameText.Text = employee.firstname;
            roleText.Text = employee.lastname;
            employeeIdText.Text = employee.empcode.ToString();
            emailAddressText.Text = employee.email;
            var imageBitmap = new DownloadImage().GetImageBitmapFromUrl("http://192.168.1.118:3000" + employee.profileImage);
            profileImage.SetImageBitmap(imageBitmap);
            anim = AnimationUtils.LoadAnimation(ApplicationContext,
                           Resource.Animation.scale_animation);
            profileImage.StartAnimation(anim);
            viewPager = FindViewById<ViewPager>(Resource.Id.viewPager);
            myPagerAdapter = new MyPagerAdapter(SupportFragmentManager, employeeId);
            viewPager.Adapter = myPagerAdapter;
            viewPager.SetPageTransformer(true, new ScaleTransformer());
        }
                
    }

}