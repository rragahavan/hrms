
using Android.App;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.View;
using Android.Widget;
using HRMS.Classes;
using HRMS.Controller;
using Shared.Adapters;
using Shared.Fragments;
using System;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;

namespace HRMS.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo",Label = "TmsActivity")]
    public class TmsActivity : NavigationDrawerActivity
    {
        TextView toolbarTitle;
        EditText startDate,endDate;
        //ViewPager viewPager;
        //LeaveAdapter leaveAdapter;
        //private int employeeId = 2;
        TextView dateDisplayText;
        

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Tms);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.hamburgers);
            set(Resources.GetStringArray(Resource.Array.nav_drawer_items));
            toolbarTitle = FindViewById<TextView>(Resource.Id.toolbarTitle);
            toolbarTitle.Text = Resources.GetString(Resource.String.Tms);
            startDate = FindViewById<EditText>(Resource.Id.startDateText);
            endDate = FindViewById<EditText>(Resource.Id.endDateText);
            startDate.Click += StartDate_Click;
            endDate.Click += EndDate_Click;
           
            //dateDisplayText= FindViewById<TextView>(Resource.Id.dateDisplayText);
            Spinner leaveTypeSpinner = FindViewById<Spinner>(Resource.Id.typeOfLeave);
            leaveTypeSpinner.ItemSelected += LeaveTypeSpinner_ItemSelected;
            var adapter = ArrayAdapter.CreateFromResource(
                this, Resource.Array.leave_types, Android.Resource.Layout.SimpleSpinnerItem);

            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            leaveTypeSpinner.Adapter = adapter;
            //viewPager = FindViewById<ViewPager>(Resource.Id.viewPager);
            //leaveAdapter = new LeaveAdapter(SupportFragmentManager, employeeId);
            //viewPager.Adapter = leaveAdapter;
            //viewPager.SetPageTransformer(true, new ScaleTransformer());
            //viewPager.Click += ViewPager_Click;

        }

        private void EndDate_Click(object sender, EventArgs e)
        {
            DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
            {
                endDate.Text = time.ToLongDateString();
            });
            frag.Show(FragmentManager, "Hai");
        }

        private void StartDate_Click(object sender, EventArgs e)
        {
            DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
            {
                startDate.Text = time.ToLongDateString();
            });
            frag.Show(FragmentManager, "Hai");
        }

        private void LeaveTypeSpinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            
        }

        
        //private void ViewPager_Click(object sender, System.EventArgs e)
        //{
        //    throw new System.NotImplementedException();
        //}
    }
}