
using Android.App;
using Android.OS;
using Android.Widget;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;

namespace HRMS.Views
{
    [Activity(Label = "KmsActivity", Theme = "@style/Theme.DesignDemo")]
    public class KmsActivity : NavigationDrawerActivity
    {
        TextView toolbarTitle;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Kms_layout);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.hamburgers);
            set(Resources.GetStringArray(Resource.Array.nav_drawer_items));
            toolbarTitle = FindViewById<TextView>(Resource.Id.toolbarTitle);
            toolbarTitle.Text = Resources.GetString(Resource.String.Kms);
        }

    }
}