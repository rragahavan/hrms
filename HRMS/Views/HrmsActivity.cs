using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Shared.Controller;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.Widget;
using Shared.Model;
using HRMS.Classes;

namespace HRMS.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "HrmsActivity")]
    public class HrmsActivity : NavigationDrawerActivity
    {

        List<Employees> employeeDetails;
        RecyclerView mRecyclerView;
        RecyclerView.LayoutManager mLayoutManager;
        EmployeeAdapter mAdapter;
        TextView toolbarTitle;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.hrms);
            set(Resources.GetStringArray(Resource.Array.nav_drawer_items));
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            toolbarTitle = FindViewById<TextView>(Resource.Id.toolbarTitle);
            toolbarTitle.Text = Resources.GetString(Resource.String.Hrms);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.hamburgers);
            employeeDetails = new EmployeeDetails().getEmployee();

            mRecyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
            mLayoutManager = new LinearLayoutManager(this);
            mRecyclerView.SetLayoutManager(mLayoutManager);
            mAdapter = new EmployeeAdapter(employeeDetails);
            mAdapter.ItemClick += OnItemClick;
            mRecyclerView.SetAdapter(mAdapter);

        }

        void OnItemClick(object sender, int position)
        {
            Intent i = new Intent(this, typeof(EmployeeDetailsActivity));
            i.PutExtra("position", position);
            i.PutExtra("employeeId", employeeDetails[position].empcode);
            StartActivity(i);
            OverridePendingTransition(Resource.Animation.activity_open_translate, Resource.Animation.activity_close_scale);

        }

        public class EmployeeViewHolder : RecyclerView.ViewHolder
        {
            public ImageView profileImage { get; private set; }
            public TextView firstName { get; private set; }
            public TextView designation { get; private set; }
            public TextView emailAddress { get; private set; }
            public EmployeeViewHolder(View itemView, Action<int> listener)
                : base(itemView)
            {

                profileImage = itemView.FindViewById<ImageView>(Resource.Id.imageView);
                firstName = itemView.FindViewById<TextView>(Resource.Id.textView);
                designation = itemView.FindViewById<TextView>(Resource.Id.designationText);
                emailAddress = itemView.FindViewById<TextView>(Resource.Id.emailAddressText);
                itemView.Click += (sender, e) => listener(base.Position);
            }
        }

        public class EmployeeAdapter : RecyclerView.Adapter
        {
            public event EventHandler<int> ItemClick;
            public List<Employees> mEmployee;

            public EmployeeAdapter(List<Employees> employee)
            {
                mEmployee = employee;
            }


            public override RecyclerView.ViewHolder
                OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                View itemView = LayoutInflater.From(parent.Context).
                            Inflate(Resource.Layout.RecyclerItems, parent, false);
                EmployeeViewHolder vh = new EmployeeViewHolder(itemView, OnClick);
                return vh;
            }

            public override void
                OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {
                EmployeeViewHolder vh = holder as EmployeeViewHolder;
                var emp = mEmployee[position];
                var imageBitmap = new DownloadImage().GetImageBitmapFromUrl("http://192.168.1.118:3000" + emp.profileImage);
                vh.profileImage.SetImageBitmap(imageBitmap);
                vh.firstName.Text = emp.firstname;
                vh.designation.Text = emp.lastname;
                vh.emailAddress.Text = emp.email;

            }


            public override int ItemCount
            {
                get { return mEmployee.Count; }
            }


            void OnClick(int position)
            {
                if (ItemClick != null)
                    ItemClick(this, position);
            }
        }
    }
}