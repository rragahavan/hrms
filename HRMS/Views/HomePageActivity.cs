﻿using Android.App;
using Android.Widget;
using Android.OS;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using HRMS.Views;

namespace HRMS
{
    [Activity(Label = "HRMS", Theme = "@style/Theme.DesignDemo", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : NavigationDrawerActivity
    {


        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.HomePage);
            set(Resources.GetStringArray(Resource.Array.nav_drawer_items));
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            var toolbarTitle = FindViewById<TextView>(Resource.Id.toolbarTitle);
            toolbarTitle.SetText(Resource.String.Home);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.hamburgers);
            
        }
        public override void OnBackPressed()
        {
            Finish();

        }

    }
}

